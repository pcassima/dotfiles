[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=Horizon
Font=Hack Nerd Font,14,-1,5,400,0,0,0,0,0,0,0,0,0,0,1
TabTitle=%d - %w
UseFontLineChararacters=false

[General]
ErrorBackground=2
LocalTabTitleFormat=%d - %w
Name=Custom
Parent=/usr/share/konsole/Breath.profile
TerminalColumns=164
TerminalRows=32
