# Dotfiles

All my dotfiles for linux :D

Also check out my collection of [snippets](https://git.transistories.org/pcassima/snippets "snippets repository") and [utilities](https://git.transistories.org/pcassima/utilities "utilities repository").

## Installation

1. Clone this repo, somewhere where you will be able to find it easily.
1. Use symbolic links to "copy" the files to your home directory or wherever you want to use them.

## Mapping

| Repo | Filesystem |
| ---- | ---------- |
| KDE/Konsole | .local/share/konsole |
