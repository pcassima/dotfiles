set nocompatible
filetype on
filetype plugin on
filetype indent on
syntax on

set expandtab

set shiftwidth=4
set tabstop=4
set softtabstop=4
set incsearch

set showmode
set hlsearch
set showmatch

set wildmenu
set wildmode=list:longest

set autoread

"set cursorline

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()
Plug 'mattn/emmet-vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'Lokaltog/powerline'
" Plug 'python-mode/python-mode', { 'for': 'python', 'branch': 'develop' }

Plug 'godlygeek/tabular'
Plug 'preservim/vim-markdown'
call plug#end()

"Emmet is now triggerd with ,, <comma comma>
let g:user_emmet_leader_key=','
"let g:airline_theme='minimalist'
let g:airline_theme='wombat'
"let g:airline#extensions#tabline#enabled=1
"let g:airline#extensions#tabline#formatter='unique_tail'
let g:airline_powerline_fonts=1


